import Vue from 'vue'
import Router from 'vue-router'
import Overview from '@/components/Overview'
import NotFound from '@/components/NotFound'
import Senders from '@/components/Senders'
import Keywords from '@/components/Keywords'
import Design from '@/components/Design'
import Option from '@/components/Options'
import EmailD from '@/components/EmailD'
import SettingIcon from '@/components/setting_icon'
import SendIcon from '@/components/send_icon'
import SideIcon from '@/components/side_icon'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/Overview',
            name: 'Overview',
            component: Overview
        },
        {
            path: '/senders',
            name: 'Senders',
            component: Senders
        },
        {
            path: '/Keywords',
            name: 'Keywords',
            component: Keywords
        },
        {
            path: '/Design',
            name: 'Design',
            component: Design
        },
        {
            path: '/EmailD',
            name: 'EmailD',
            component: EmailD
        },
        {
            path: '/SettingIcon',
            name: 'SettingIcon',
            component: SettingIcon
        },
        {
            path: '/SendIcon',
            name: 'SendIcon',
            component: SendIcon
        },
        {
            path: '/SideIcon',
            name: 'SideIcon',
            component: SideIcon
        },
        {
            path: '/Option',
            name: 'Option',
            component: Option
        },
        {
            path: '*',
            name: 'NotFound',
            component: NotFound
        },

    ]
})